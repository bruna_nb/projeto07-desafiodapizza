var btCadastro = document.querySelector("#idBtCadastro")
var listaPizzas = []
const pi = 3.141516
var btRelatorio = document.querySelector("#idBtRelatorio")
var btVoltar = document.querySelector("#idBtVoltar")
btVoltar.style.display = "none"
var btRecomecar = document.querySelector("#idBtRecomecar")
btRecomecar.style.display = "none"
var divsEsconder = document.getElementsByClassName("divs-esconder")
var divExibirMensagem = document.querySelector("#idDivExibirMensagem")
var divExibirRelatorio = document.querySelector("#idDivExibirRelatorio")
divExibirRelatorio.style.display = "none"
var tabela = document.querySelector("#idtBody")


btCadastro.addEventListener("click", function(event){
    event.preventDefault()
    let verificacao = verificaCriterios()

    if(verificacao){
        let form = document.getElementById("idFormCadastro")
        let pizza = construirElemPizza(form)
        pizza.precoPorArea = calcularPrecoPorArea(pizza.tamanho,pizza.preco)
        listaPizzas.push(pizza)
        if(listaPizzas.length == 1){
            btRelatorio.disabled = false
            btRelatorio.classList.remove("btn-secondary")
            btRelatorio.classList.add("btn-warning")
        }
        form.reset()
        let msgConfirmacao = "<p>Pizza cadastrada com sucesso! Clique em <b>'Voltar'</b> para cadastrar uma nova pizza ou em <b>'Gerar relatório'</b> para ver qual é a pizza mais barata!</p>"
        mostrarMensagem(msgConfirmacao)
    }
})

function verificaCriterios(){
    let verificacao = true
    let nomePizza = document.querySelector('#idNomePizza')
    let tamPizza = document.querySelector("#idTamanhoPizza").value
    let valPizza = document.querySelector("#idPrecoPizza").value
    let precoPizza = valPizza.replace(",",".")
    let mensagem = "<p>Pizza não cadastrada. ERRO(s): </p>"
    if(isNaN(precoPizza)){
        mensagem += "<p>**001. O Preço da pizza deve ser um número.Ex.: 20,00 ou 35,50 ou 10.00 ou 20.80.</p>"
        verificacao = false
    }
    for (let pizza of listaPizzas){
        if(tamPizza == pizza.tamanho){
            mensagem += "<p>**002. Já existe uma pizza com este tamanho cadastrada.</p>"
            verificacao = false
        }
    }
    if(nomePizza=="" || tamPizza=="" || valPizza==""){
        mensagem += "<p>**003. Algum dos campos do formulário ficou vazio.</p>"
        verificacao = false
    }
    if(!verificacao){
        mensagem += "<p>Clique em <b>'Voltar'</b> para tentar novamente.</p>"
        mostrarMensagem(mensagem)
    } 
    return verificacao
}

function construirElemPizza(form){
    let pizza = {
        nome: form.nmNomePizza.value,
        tamanho: form.nmTamPizza.value,
        preco: form.nmPrecoPizza.value,
        precoPorArea:"",
        diferenca: ""
    }
    return pizza
}

function calcularPrecoPorArea(tam, preco){
    return (preco / ((pi*tam**2)/4))
}

function mostrarMensagem(mensagem){
    btVoltar.style.display="inline"
    btCadastro.style.display="none"
    esconderFormulario() 
    divExibirMensagem.innerHTML = mensagem

}

btVoltar.addEventListener("click",function(){
    btVoltar.style.display="none"
    btCadastro.style.display="inline"
    for(let div of divsEsconder){
        div.style.display="flex"
    }
    divExibirMensagem.innerHTML=""
})

function esconderFormulario(){
    for(let div of divsEsconder){
        div.style.display="none"
    }
}

btRelatorio.addEventListener("click", function(){
    let listaOrdenada = ordenarLista()
    preencherTabela(listaOrdenada)
    btRelatorio.style.display="none"
    btCadastro.style.display="none"
    btVoltar.style.display="none"
    btRecomecar.style.display="inline"
    divExibirMensagem.style.display="none"
    divExibirRelatorio.style.display="flex"
    document.getElementById("idFormCadastro").classList.remove("col-5")
    document.getElementById("idFormCadastro").classList.add("col-7")
    esconderFormulario()
})

function preencherTabela(listaOrdenada){
    calcularDiferencas(listaOrdenada)
    for(let pizza of listaOrdenada){
        let trow = document.createElement("tr")
        trow.appendChild(inserirValorNaCelula(pizza.nome))
        trow.appendChild(inserirValorNaCelula(pizza.tamanho))
        trow.appendChild(inserirValorNaCelula(pizza.preco))
        console.log(pizza.precoPorArea)
        trow.appendChild(inserirValorNaCelula("R$ "+pizza.precoPorArea.toFixed(2)))
        trow.appendChild(inserirValorNaCelula(pizza.diferenca))
        tabela.appendChild(trow)
    }
}
function inserirValorNaCelula(valor){
    let td = document.createElement("td")
    td.textContent = valor
    return td
}

function calcularDiferencas(listaOrdenada){
    listaOrdenada[0].diferenca = "Melhor CB"
    for(let i=1; i < listaOrdenada.length; i++){
        listaOrdenada[i].diferenca = (((listaOrdenada[i].precoPorArea / listaOrdenada[i-1].precoPorArea)-1)*100).toFixed(2) + "%"
    }
}

function ordenarLista(){
    let listaOrd=[]
    if(listaPizzas.length == 1){
        listaOrd=listaPizzas
    }else{
        while(listaPizzas.length > 0){
            let menor = listaPizzas[0]
            let imenor = 0
            for(let i=0; i<listaPizzas.length;i++){
                if (listaPizzas[i].precoPorArea < menor.precoPorArea){
                    menor = listaPizzas[i]
                    imenor = i
                }
            }
            listaOrd.push(menor)
            listaPizzas.splice(imenor,1)
        }
    }
    return listaOrd
}
btRecomecar.addEventListener("click", function(){
    location.href='index.html'
})